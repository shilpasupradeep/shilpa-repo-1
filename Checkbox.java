import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Checkbox {

	public static void main(String[] args) {

		System.setProperty("webdriver.gecko.driver", "/Users/shilpasupradeep/AutomationTesting/Drivers/Firefox/geckodriver");
		WebDriver driver = new FirefoxDriver();
		
		 //Puts a Implicit wait, Will wait for 10 seconds before throwing exception
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		//Launch website
		driver.navigate().to("http://www.calculator.net/mortgage-calculator.html");
		driver.manage().window().maximize();
		
	    //Click on check Box
		driver.findElement(By.id("caddoptional")).click();
		driver.findElement(By.id("cpropertytaxes")).clear();
		driver.findElement(By.id("cpropertytaxes")).sendKeys("1.5");
		
		//driver.close();
		
		
		
	}

}
