import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Multiselect {

	public static void main(String[] args) throws InterruptedException {
		 System.setProperty("webdriver.gecko.driver", "/Users/shilpasupradeep/AutomationTesting/Drivers/Firefox/geckodriver");
		 WebDriver driver = new FirefoxDriver();
		 WebDriverWait myWaitVar = new WebDriverWait(driver,20);
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 driver.get("http://demos.devexpress.com/aspxeditorsdemos/ListEditors/MultiSelect.aspx");
		 driver.manage().window().maximize();
		 
		 //changed the element ID
		 driver.findElement(By.id("ControlOptionsTopHolder_lbSelectionMode_I")).click();
		 
	     Thread.sleep(2000);
	     //changed the element ID
		 driver.findElement(By.id("ControlOptionsTopHolder_lbSelectionMode_DDD_L_LBI1T0")).click();
	       
	      // Perform Multiple Select
	      Actions builder = new Actions(driver);
	      WebElement select = driver.findElement(By.id("ContentHolder_lbFeatures_LBT"));
	      List<WebElement> options = select.findElements(By.tagName("td"));
	      
	      System.out.println(options.size());
	      //changed from Control to Command as its MAC
	      Action multipleSelect = builder.keyDown(Keys.COMMAND).click(options.get(2)).click(options.get(4)).click(options.get(6)).build();
	    		  
	      multipleSelect.perform();
	      
	      Thread.sleep(10000);
	      driver.close();

		 
	}

}
