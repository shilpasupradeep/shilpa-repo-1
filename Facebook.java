import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Facebook {
	
	public static void main(String...args) {
		System.setProperty("webdriver.gecko.driver", "/Users/shilpasupradeep/AutomationTesting/Drivers/Firefox/geckodriver");
		FirefoxDriver driver = new FirefoxDriver();
		driver.get("https://www.facebook.com");
		
		String tagName = driver.findElement(By.id("email")).getTagName();
		System.out.println(tagName);
		driver.findElement(By.id("email")).sendKeys("testuser@gmail.com");
		driver.findElement(By.id("pass")).sendKeys("abcde");
		driver.findElement(By.id("loginbutton")).click();

		driver.close();
		System.exit(0);
	}

}
